/**
 * 
 */
package com.dak.weibo;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * @author FMK
 * 2013年12月17日上午11:20:51
 *
 */
public class TestFilePath {

	public static void main(String[] args) throws Exception {
		URL url = TestFilePath.class.getClassLoader().getResource("pic.json");

		System.out.println(url.toString());
		InputStream is = url.openStream();
		
		FileOutputStream f = new FileOutputStream(new File("d:/dd.json"));
		BufferedInputStream bis = new BufferedInputStream(is);
		BufferedOutputStream bos = new BufferedOutputStream(f);
		
		
		Long s = System.currentTimeMillis();
		int i = 0;
		byte[] b = new byte[1024];
		while ((i = bis.read(b)) != -1) {
			bos.write(b);
		}
		bos.flush();
		System.out.println(System.currentTimeMillis()- s);
//		File file = new File("src/main/resources/pic.json");
//		System.out.println(file.exists());
//		Configuration configuration = new PropertiesConfiguration("config.properties");
//		String name = configuration.getString("username");
//		System.out.println(name);
//		Properties p = new Properties();
//		p.load(new FileInputStream(new File("src/main/resources/config.properties")));
//		System.out.println(p.get("password") + "---");
	}
}
