/**
 * 
 */
package com.dak.weibo.jsonentity;

public class Data {
	private String album_id;
	private Integer total;
	private Photo[] photo_list;

	public String getAlbum_id() {
		return album_id;
	}

	public void setAlbum_id(String album_id) {
		this.album_id = album_id;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Photo[] getPhoto_list() {
		return photo_list;
	}

	public void setPhoto_list(Photo[] photo_list) {
		this.photo_list = photo_list;
	}

}