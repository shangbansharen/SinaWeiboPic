/**
 * 
 */
package com.dak.weibo.jsonentity;

import java.util.Date;

/**
 * @author FMK 2013年12月17日上午10:30:36
 * 
 */
@SuppressWarnings("unused")
public class Picture {

	private Boolean result;
	private Integer code;
	private String msg;
	private Date timestamp;
	private Data data;

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
}
