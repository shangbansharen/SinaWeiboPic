/**
 * 
 */
package com.dak.weibo.jsonentity;

import java.util.Date;

public class Photo {
	private String photo_id;
	private String album_id;
	private String uid;
	private String status;
	private Integer type;
	private String property;
	private String source;
	private String pid;
	private Integer original_time;
	private String caption;
	private String tags;
	private Integer latitude;
	private Integer longtitude;
	private String geo_description;
	private String updated_at;
	private String created_at;
	private String data;
	private String pic_host;
	private String pic_name;
	private Integer pic_type;
	private String pic_pid;
	private String from;
	private Boolean is_favorited;
	private Boolean is_liked;
	private Boolean is_cover;
	private Date timestamp;
	private String caption_render;
	private Photo.Count count;

	private static class Count {
		private Integer clicks;
		private Integer retweets;
		private Integer likes;
		private Integer comments;

		public Integer getClicks() {
			return clicks;
		}

		public void setClicks(Integer clicks) {
			this.clicks = clicks;
		}

		public Integer getRetweets() {
			return retweets;
		}

		public void setRetweets(Integer retweets) {
			this.retweets = retweets;
		}

		public Integer getLikes() {
			return likes;
		}

		public void setLikes(Integer likes) {
			this.likes = likes;
		}

		public Integer getComments() {
			return comments;
		}

		public void setComments(Integer comments) {
			this.comments = comments;
		}
	}

	public String getPhoto_id() {
		return photo_id;
	}

	public void setPhoto_id(String photo_id) {
		this.photo_id = photo_id;
	}

	public String getAlbum_id() {
		return album_id;
	}

	public void setAlbum_id(String album_id) {
		this.album_id = album_id;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Integer getOriginal_time() {
		return original_time;
	}

	public void setOriginal_time(Integer original_time) {
		this.original_time = original_time;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Integer getLatitude() {
		return latitude;
	}

	public void setLatitude(Integer latitude) {
		this.latitude = latitude;
	}

	public Integer getLongtitude() {
		return longtitude;
	}

	public void setLongtitude(Integer longtitude) {
		this.longtitude = longtitude;
	}

	public String getGeo_description() {
		return geo_description;
	}

	public void setGeo_description(String geo_description) {
		this.geo_description = geo_description;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getPic_host() {
		return pic_host;
	}

	public void setPic_host(String pic_host) {
		this.pic_host = pic_host;
	}

	public String getPic_name() {
		return pic_name;
	}

	public void setPic_name(String pic_name) {
		this.pic_name = pic_name;
	}

	public Integer getPic_type() {
		return pic_type;
	}

	public void setPic_type(Integer pic_type) {
		this.pic_type = pic_type;
	}

	public String getPic_pid() {
		return pic_pid;
	}

	public void setPic_pid(String pic_pid) {
		this.pic_pid = pic_pid;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public Boolean getIs_favorited() {
		return is_favorited;
	}

	public void setIs_favorited(Boolean is_favorited) {
		this.is_favorited = is_favorited;
	}

	public Boolean getIs_liked() {
		return is_liked;
	}

	public void setIs_liked(Boolean is_liked) {
		this.is_liked = is_liked;
	}

	public Boolean getIs_cover() {
		return is_cover;
	}

	public void setIs_cover(Boolean is_cover) {
		this.is_cover = is_cover;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getCaption_render() {
		return caption_render;
	}

	public void setCaption_render(String caption_render) {
		this.caption_render = caption_render;
	}

	public Photo.Count getCount() {
		return count;
	}

	public void setCount(Photo.Count count) {
		this.count = count;
	}
}