/**
 * 
 */
package com.dak.weibo;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import com.dak.weibo.jsonentity.Photo;
import com.dak.weibo.jsonentity.Picture;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author FMK 2013年12月17日上午10:27:59
 * 
 */
public class Test {

	public static void main(String[] args) throws Exception {
		Long start = System.currentTimeMillis();
		ObjectMapper mapper = new ObjectMapper();
		File file = new File("d:/pic.json");
//		System.out.println(file.getName());
		Picture p = mapper.readValue(file, Picture.class);
		for (Photo pp : p.getData().getPhoto_list()) {
			// System.out.println(pp.getPic_host());
			// System.out.println(pp.getPic_name());
			String url2 = pp.getPic_host() + "/large/" + pp.getPic_name();
			URL url = new URL(url2);
//			URLConnection uc = url.openConnection();
//			InputStream is = uc.getInputStream();
			BufferedInputStream is = new BufferedInputStream(url  
            .openStream());  
			File f = new File("d:/test");
			FileOutputStream out = new FileOutputStream(f+"/"+pp.getPic_name());
			BufferedOutputStream bos = new BufferedOutputStream(out);
			int i = 0;
			byte[] b = new byte[1024];
			while ((i = is.read(b)) != -1) {
				bos.write(b,0,i);
			}
			bos.flush();
			is.close();
		}
//		mapper.writeValue(new File("d:/tt.json"), p);
		System.out.println(System.currentTimeMillis() - start);
	}
}
